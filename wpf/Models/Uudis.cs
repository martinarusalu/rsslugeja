﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wpf.Models
{
    public class Uudis
    {
        private string _pealkiri;

        public string Pealkiri
        {
            get { return _pealkiri; }
            set { _pealkiri = value; }
        }
    }
}
