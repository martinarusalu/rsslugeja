﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Xml.Linq;
using wpf.Models;

namespace wpf.ViewModels
{
    public class MainWindowVM
    {
        private ObservableCollection<Uudis> _uudised;
        private string _rssUrl;

        public ObservableCollection<Uudis> Uudised
        {
            get
            {
                return _uudised;
            }
        }

        public MainWindowVM(string rssUrl)
        {
            _uudised = new ObservableCollection<Uudis>();
            _rssUrl = rssUrl;
        }

        public void LaeAndmed()
        {
            XDocument xdoc = XDocument.Load(_rssUrl);

            IEnumerable<XElement> query = from x in xdoc.Descendants("item")
                                          select x;
            foreach (var item in query)
            {
                XElement xTitle = item.Element("title");
                if (xTitle != null)
                {
                    Uudis uusUudis = new Uudis() {Pealkiri = xTitle.Value};
                    _uudised.Add(uusUudis);
                }

                XElement xDescription = item.Element("description");
                if (xDescription != null)
                {
                }
            }
        }
    }
}
