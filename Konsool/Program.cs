﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Konsool
{
    class Program
    {
        static void Main(string[] args)
        {
            XDocument xdoc = XDocument.Load("http://www.postimees.ee/rss/");

            IEnumerable<XElement> query = from x in xdoc.Descendants("item")
                select x;
            foreach (var item in query)
            {
                XElement xTitle = item.Element("title");
                if (xTitle != null)
                {
                    Console.WriteLine(xTitle.Value);
                }

                XElement xDescription = item.Element("description");
                if (xDescription != null)
                {
                    Console.WriteLine(xDescription.Value + "\n\n");
                }
            }
            Console.ReadKey();
        }
    }
}
